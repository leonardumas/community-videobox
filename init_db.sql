CREATE OR REPLACE DATABASE cvideobox;
GRANT ALL PRIVILEGES ON cvideobox.* TO 'cvideobox' IDENTIFIED BY 'cvideobox';
CREATE OR REPLACE TABLE cvideobox.votes (choice INT, dt DATETIME);