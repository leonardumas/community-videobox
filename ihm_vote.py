import tkinter as tk
import mysql.connector
from datetime import datetime

# params db

mydb = mysql.connector.connect(
    host = 'localhost',
    user = 'cvideobox',
    password = 'cvideobox',
    database = 'cvideobox'
)

# enregistrement du vote

def registerVote(choice):
    mycursor = mydb.cursor()
    sql = 'INSERT INTO votes (choice, dt) VALUES (%s, %s)'
    val = (choice, datetime.now())
    mycursor.execute(sql, val)
    mydb.commit()
    print(mycursor.rowcount, 'record inserted.')

# init window

window = tk.Tk()

# boutons

def createButton(value, photo):
    button = tk.Button(window, text = value, 
        command = lambda: registerVote(value), 
        image = photo
    ).pack()

# fullscreen

fullScreenState = True

def toggleFullScreen(event):
    global fullScreenState # pour éviter une UnboundLocalError
    fullScreenState = not fullScreenState
    window.attributes('-fullscreen', fullScreenState)

window.attributes('-fullscreen', fullScreenState)
window.bind('<F11>', toggleFullScreen)

# init IHM

tk.Label(window, text = 'Faites votre choix !', font =(
'Verdana', 15)).pack(side = tk.TOP, pady = 10)

photo1 = tk.PhotoImage(file = '1.png')
photo2 = tk.PhotoImage(file = '2.png')

for value, photo in [(1, photo1), (2, photo2)]:
    createButton(value, photo)

window.mainloop()