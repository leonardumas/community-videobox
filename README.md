# Community VideoBox

## IHM et BDD des votes

1. initialiser la BDD des votes en exécutant dans le client SQL les instructions de `init_db.sql`
2. lancer le script `ihm_vote.py` (plein écran par défaut, pour réduire la fenêtre appuyer sur F11)
3. vérifier dans le client SQL que les votes sont bien enregistrés : `SELECT * FROM cvideobox.votes;`

## TODO

1. lancer le script à la réception de la tension venant de la 1ère carte
2. renvoyer une tension à la 1ère carte après le vote et clore l'IHM
3. customiser les boutons (images) et styler l'IHM

## Libs GPIO
- https://pypi.org/project/RPi.GPIO/
- https://gpiozero.readthedocs.io
